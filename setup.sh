#!/bin/bash
# Einfaches setup.sh um eine LinuxMint 17 Mate-Installation
# für den Einsatz als VM im Studiengang ET zu konfigurieren
#
# Autor: Ingo Haschler <lehre@ingohaschler.de>

#
# TBD: Reihenfolge prüfen
#

# Ideen
# Ganz von vorne: http://askubuntu.com/questions/122505/how-do-i-create-completely-unattended-install-for-ubuntu

# Partner-Repository aktivieren
sudo sed -i "/^# deb .*partner/ s/^# //" /etc/apt/sources.list 
sudo apt-get -qq update

# Sprachpakete installieren und Sprache auf deutsch umstellen
# /etc/environment, /etc/default/locale
# s. https://help.ubuntu.com/community/Locale#Changing_settings_permanently
# und http://wiki.ubuntuusers.de/Spracheinstellungen
sudo apt-get -y install language-pack-de language-pack-gnome-de language-pack-kde-de kde-l10n-de
sudo update-locale LANG=de_DE.UTF-8 LC_MESSAGES=de_DE.UTF-8
# /etc/default/console-setup
sudo sed -i "/^XKBMODEL.*/ s/evdev/pc105/" /etc/default/console-setup
sudo sed -i "/^XKBLAYOUT.*/ s/us/de/" /etc/default/console-setup
# und die Zeitzone
# http://superuser.com/questions/498330/changing-timezone-with-dpkg-reconfigure-tzdata-and-debconf-set-selections
sudo -i 'echo "Europe/Berlin" > /etc/timezone'
sudo dpkg-reconfigure -f noninteractive tzdata

# Hier vielleicht noch ein dist-upgrade?


# VORHER: Kann man mysql mit geg. root-pw installieren?
# Ja :-) (http://stackoverflow.com/questions/7739645/install-mysql-on-ubuntu-without-password-prompt)
DEBIAN_FRONTEND=noninteractive sudo -E apt-get -q -y install mysql-server
mysqladmin -u root password 123456

# Ist hier die richtige Position in der Reihenfolge?
dpkg --set-selections < packages.list
#dselect
apt-get -y update
apt-get dselect-upgrade

# Code::Blocks aus backports
# https://launchpad.net/~ubuntu-backports-testers/+archive/ppa
#sudo add-apt-repository -y ppa:ubuntu-backports-testers/ppa
#sudo apt-get -qq update
sudo apt-get install -y codeblocks codeblocks-contrib

# Zusätzliche Pakete installieren
# TBD: Mit package.list abgleichen; sind größtenteils schon enthalten...
# C(++) und Co.
sudo apt-get install -y build-essential 
# SQLite
sudo apt-get install -y sqlite3 sqlitebrowser
# MySQL
sudo apt-get install -y libmysql++-dev emma libmysqlclient-dev libmysqld-dev libmysql++-dev libmysql++-doc libmysql++3 mysql-client mysql-workbench
# SE
sudo apt-get install -y libunittest++-dev libtool automake meld doxygen umbrello
# Latex
sudo apt-get install -y texmaker texlive-lang-german texlive-extra-utils texlive-bibtex-extra jabref texlive-fonts-recommended texlive-latex-recommended texlive-generic-extra
# Sonstiges
sudo apt-get install -y curl gnuplot wireshark ngspice ngspice-doc seahorse vim oregano dconf-cli gnome-do gnome-do-plugins

# Schrift SourceCodePro installieren
# http://askubuntu.com/questions/193072/how-to-use-the-new-adobe-source-code-pro-font
mkdir /tmp/adobefont
pushd /tmp/adobefont
wget http://downloads.sourceforge.net/project/sourcecodepro.adobe/SourceCodePro_FontsOnly-1.017.zip
unzip -o -j SourceCodePro_FontsOnly-1.017.zip
mkdir -p ~/.fonts
cp *.otf ~/.fonts
popd
fc-cache -f -v
# und aktivieren
# geht neuerdings mit dconf:
# http://wiki.ubuntuusers.de/GNOME_Konfiguration/dconf
dconf write /org/mate/terminal/profiles/default/font "'Source Code Pro Regular 12'"
dconf write /org/mate/terminal/profiles/default/use-system-font false

# Evtl. vmware-toolbox per Autostart
#echo "vmware-toolbox & >> ~/.xsession"

# Optional: Password-Prompt
# Dazu neue Datei pwfeedback in /etc/sudoers.d mit dem Inhalt:
# Defaults pwfeedback

# Sonstiges:
# TBD

# Einstellungen für Gnome-Do
gconftool -s /apps/gnome-do/preferences/Do/Platform/Linux/TrayIconPreferences/StatusIconVisible -t boolean 1
gconftool -s /apps/gnome-do/preferences/Do/CorePreferences/QuietStart -t boolean 1

# Einstellungen für Umbrello
mkdir -p ~/.kde/share/config
cp ./umbrellorc ~/.kde/share/config/umbrellorc

# Verknüpfung für Proxy-Einstellungen
cp /usr/share/applications/mate-network-properties.desktop ~/Schreibtisch
chmod +x ~/Schreibtisch/mate-network-properties.desktop

# Alte Kernels automatisch entfernen
#sudo sed -i "/linux-image/ s/^/#/" /etc/apt/apt.conf.d/01autoremove
#sudo sed -i "/Remove-Unused-Dependencies/ s/^\/\///" /etc/apt/apt.conf.d/50unattended-upgrades

# Release-Updates nicht anzeigen
#sudo sed -i "/Prompt/ s/^.*$/Prompt=never/" /etc/update-manager/release-upgrades

## AVR-GCC installieren
sudo apt-get install -y gcc-avr avr-libc
mkdir ~/bin
cp ./bin/* ~/bin
chmod +x ~/bin/*
sudo usermod -a -G dialout student

# dotfiles installieren
cd $HOME
#ln -sb dotfiles/.screenrc .
ln -sb et-vm/.bash_profile .
ln -sb et-vm/.bashrc .
ln -sb et-vm/.bashrc_custom .
#ln -sf dotfiles/.emacs.d .

