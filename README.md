et-vm.git
=========
Ein frisches LinuxMint 17 (Mate) installieren. Dann klonen und ausführen, um die Arbeitsumgebung einzurichten:

```sh
cd $HOME
sudo apt-get install -y git-core
git clone https://gitlab.com/ingoha/et-vm.git
cd et-vm
git checkout linuxmint
./setup.sh   
```

Die Referenz-Instanz wurde mit diesem (http://www.linuxmint.com/edition.php?id=159) Installationsmedium erzeugt, auf der im Anschluß zuerst alle Pakete aktualisiert wurden. Als Benutzer wurde student mit dem Paßwort 123456 eingestellt.

Wird das Image auf einer USB-Platte betrieben, sollte in der Konfigurationsdatei (Ubuntu.vmx) folgende Zeile eingefügt werden:
```mainMem.useNamedFile = "FALSE"``` (s. http://kb.vmware.com/selfservice/microsites/search.do?language=en_US&cmd=displayKC&externalId=1620)
